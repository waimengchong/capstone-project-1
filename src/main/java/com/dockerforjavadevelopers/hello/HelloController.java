package com.dockerforjavadevelopers.hello;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/")
    public String index() {
        return "<!DOCTYPE html><html><body><h1>Justin Chong Wai Meng</h1><button type=\"button\" onclick=\"document.getElementById('datetime').innerHTML = Date()\">Click me to display Date and Time.</button><p id=\"datetime\"></p></body></html>";
    }

}
